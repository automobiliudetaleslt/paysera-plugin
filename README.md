# OmniSyliusPaysera

## How it works?

This is an plugin that enables gateway paysera for payum which configure 
with omnipay and connected with omnipay-v3-bridge. 
After install all configuration below you need only create in admin payments section type paysera, and when you
can see this payment type in order checkout.

![Paysera scheme](https://developers.paysera.com/bundles/evppayseradoc/img/macro-scheme-lt.png) 

callbackurl: `http://{localhost}/payment/notify/{notify_token}?data=foo&ss1=bar&ss2=baz`  
accepturl: `http://{localhost}/{_locale}/order/after-pay?payum_token={accept_token}`  
cancelurl: `http://{localhost}/{_locale}/account/orders/`  

## Installation

### Add the plugin to the Kernel

#### app/AppKernel.php

```yaml
$bundles = [
    new \Omni\Sylius\PayseraPlugin\OmniSyliusPayseraPlugin(),
];
```

#### app/config/config.yml

When using single Paysera project:

```yaml
omni_sylius_paysera:
    project_id: '%paysera.project_id%'
    password: '%paysera.password%'
    test_mode: '%paysera.test_mode%'
```

When using multiple Paysera projects:

```yaml
omni_sylius_paysera:
    projects:
        b2c:
            project_id: '%paysera.project_id_b2c%'
            password: '%paysera.password_b2c%'
            test_mode: '%paysera.test_mode_b2c%'
        b2b:
            project_id: '%paysera.project_id_b2b%'
            password: '%paysera.password_b2b%'
            test_mode: '%paysera.test_mode_b2b%'
```

### Start using the plugin

That's it, you can start using the plugin as described in the first 
section.

### Recommendation

It is recommended to update `@SyliusShopBundle:Order/show.html.twig` in your templates and render select-payment form
with additional `{% if form.payments|length %}` condition since there might be payments not yet marked as paid after returning
from payment system.

### Tips & tricks

#### Manual payment notify

`Notify` step that marks payment completed will not work when developing locally.
To overcome this it is possible to make 
`Omni\Sylius\PayseraPlugin\Payum\Action\ValidateAction::isValid` always return `true`.
Then just simply request the following:
`http://{localhost}/payment/notify/{notify_token}?data=foo&ss1=bar&ss2=baz`

#### portal.nfq.lt

Also you can use `portal.nfq.lt` when developing on `devops/vms` platform.
This works only in `dev` mode.
Read more about it [here](http://gitlab.nfq.lt/devops/docker-projects/wikis/testing-third-party-callbacks-in-development-using-portal).

Also the following line needs to be overwritten:

```
# vendor/payum/omnipay-v3-bridge/src/Action/OffsiteCaptureAction.php:73
$details['notifyUrl'] = str_replace('web.sylius-standard.omni.test', 'portal.nfq.lt:{YOUR_PORTAL}', $notifyToken->getTargetUrl());
```
