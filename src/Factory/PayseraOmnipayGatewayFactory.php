<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Factory;

use Omni\Sylius\PayseraPlugin\Payum\Extension\PresetPaymentMethodExtension as PresetMethod;
use Omnipay\Paysera\Gateway;
use Payum\Core\Bridge\Spl\ArrayObject;
use Payum\Core\Exception\LogicException;
use Payum\OmnipayV3Bridge\OmnipayGatewayFactory;

/**
 * Class PayseraOmnipayGatewayFactory.
 */
class PayseraOmnipayGatewayFactory extends OmnipayGatewayFactory
{
    /**
     * {@inheritdoc}
     */
    protected function populateConfig(ArrayObject $config): void
    {
        if ($config['paymentMethod']) {
            $config['payum.extension.paysera.payment_method'] = new PresetMethod($config['paymentMethod']);
        }

        $projectConfig = $this->getProjectConfig($config);

        $config['options'] = [
            'version' => Gateway::VERSION,
            'testMode' => $projectConfig['test_mode'],
            'projectId' => $projectConfig['project_id'],
            'password' => $projectConfig['password'],
        ];

        parent::populateConfig($config);
    }

    /**
     * @param ArrayObject $config
     *
     * @return array
     */
    protected function getProjectConfig(ArrayObject $config): array
    {
        $pluginConfig = $config['omni_paysera.config'];

        if ($config['project']) {
            if (isset($pluginConfig['projects'][$config['project']])) {
                return $pluginConfig['projects'][$config['project']];
            }

            throw new LogicException(sprintf('Paysera project \'%s\' is not configured.', $config['project']));
        }

        return $pluginConfig['projects']['default'];
    }
}
