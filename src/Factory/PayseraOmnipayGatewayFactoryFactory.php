<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Factory;

use Omni\Sylius\PayseraPlugin\Payum\Action\GetRequestAction;
use Omni\Sylius\PayseraPlugin\Payum\Action\GetStatusAction;
use Omni\Sylius\PayseraPlugin\Payum\Action\ValidateAction;
use Payum\Core\GatewayFactoryInterface;
use Payum\OmnipayV3Bridge\OmnipayGatewayFactory;

/**
 * Class PayseraOmnipayGatewayFactoryFactory.
 */
final class PayseraOmnipayGatewayFactoryFactory
{
    /**
     * @var array
     */
    private $config;

    /**
     * PayseraOmnipayGatewayFactoryFactory constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $defaults
     * @param GatewayFactoryInterface $coreGatewayFactory
     *
     * @return OmnipayGatewayFactory
     */
    public function __invoke(array $defaults, GatewayFactoryInterface $coreGatewayFactory): OmnipayGatewayFactory
    {
        $defaults['payum.action.paysera_get_status'] = new GetStatusAction($this->config['use_order_number']);
        $defaults['payum.action.paysera_get_request'] = new GetRequestAction();
        $defaults['payum.action.paysera_validate'] = new ValidateAction();

        $defaults['type'] = 'Paysera';
        $defaults['omni_paysera.config'] = $this->config;

        return new PayseraOmnipayGatewayFactory(null, $defaults, $coreGatewayFactory);
    }
}
