<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

final class PayseraGatewayConfigurationType extends AbstractType
{
    /**
     * @var array
     */
    private $projects = [];

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'paymentMethod',
                ChoiceType::class,
                [
                    'label' => 'omni_sylius.paysera.payment_method',
                    'choices' => [
                        'sampo',
                        'nord',
                        'parex',
                        'nordealt',
                        'vb2',
                        'hanza',
                        'sb',
                        'mb',
                        'lku',
                        'webmoney',
                        'wallet',
                        'smsbank',
                        'lt_post',
                        'lt_perlas',
                        'lt_gf_leasing',
                        'lt_mokilizingas',
                        'maximalt',
                        'barcode',
                        'lthand',
                        '1stpay',
                        'lv_lpb',
                        'moq',
                    ],
                    'choice_label' => function ($value) {
                        return 'omni_sylius.paysera.payment_methods.' . $value;
                    },
                    'placeholder' => 'omni_sylius.paysera.payment_methods.choose',
                    'required' => false,
                ]
            )
            ->add(
                'project',
                ChoiceType::class,
                [
                    'label' => 'omni_sylius.form.paysera_project',
                    'choices' => $this->projects,
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'sylius_payum_gateway_config';
    }

    /**
     * @param array $projects
     */
    public function setProjectChoices(array $projects): void
    {
        $this->projects = array_combine(array_keys($projects), array_keys($projects));
    }
}
