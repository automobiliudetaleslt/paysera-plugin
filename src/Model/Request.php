<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Model;

use Omnipay\Paysera\Common\Encoder;

/**
 * Class Request.
 */
final class Request
{
    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $ss1;

    /**
     * @var string
     */
    private $ss2;

    /**
     * @var array;
     */
    private $request;

    /**
     * ValidateResponse constructor.
     *
     * @param string $data
     * @param string $ss1
     * @param string $ss2
     */
    public function __construct(string $data, string $ss1, string $ss2)
    {
        $this->data = $data;
        $this->ss1 = $ss1;
        $this->ss2 = $ss2;
        parse_str(Encoder::decode($data), $request);
        $this->request = $request ?? [];
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getSs1(): string
    {
        return $this->ss1;
    }

    /**
     * @return string
     */
    public function getSs2(): string
    {
        return $this->ss2;
    }

    /**
     * @return array
     */
    public function getRequest(): array
    {
        return $this->request;
    }
}
