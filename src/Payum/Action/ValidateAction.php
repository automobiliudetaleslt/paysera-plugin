<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Payum\Action;

use GuzzleHttp\Psr7\Request as HttpRequest;
use Omni\Sylius\PayseraPlugin\Model\Request;
use Omni\Sylius\PayseraPlugin\Payum\Request\ValidateRequest;
use Omnipay\Paysera\Common\Encoder;
use Omnipay\Paysera\Common\SignatureGenerator;
use Payum\Core\Action\ActionInterface;
use Payum\Core\ApiAwareInterface;
use Payum\Core\ApiAwareTrait;
use Payum\Core\HttpClientInterface;

/**
 * Class ValidateAction.
 *
 * @property HttpClientInterface $api
 */
final class ValidateAction implements ActionInterface, ApiAwareInterface
{
    private const ENDPOINT = 'http://www.paysera.com/download/public.key';

    use ApiAwareTrait;

    /**
     * ValidateAction constructor.
     */
    public function __construct()
    {
        $this->apiClass = HttpClientInterface::class;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($validate): void
    {
        /** @var ValidateRequest $validate */
        $validate->setValid($this->isValid($validate->getRequest(), $validate->getPassword(), $validate->getOrderId()));
    }

    /**
     * {@inheritdoc}
     */
    public function supports($request): bool
    {
        return $request instanceof ValidateRequest;
    }

    /**
     * @param Request $request
     * @param string $password
     * @param string $orderId
     *
     * @return bool
     */
    private function isValid(Request $request, string $password, string $orderId): bool
    {
        return $this->isValidStatus($request)
            && $this->isSameOrder($request, $orderId)
            && $this->isSS1Valid($request, $password)
            && $this->isSS2Valid($request);
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isValidStatus(Request $request): bool
    {
        return $request->getRequest()['status'] === '1';
    }

    /**
     * @param Request $request
     * @param string $orderId
     *
     * @return bool
     */
    private function isSameOrder(Request $request, string $orderId): bool
    {
        return ($request->getRequest()['orderid'] ?? null) === (string)$orderId;
    }

    /**
     * @param Request $request
     * @param string $password
     *
     * @return bool
     */
    private function isSS1Valid(Request $request, string $password): bool
    {
        return SignatureGenerator::generate($request->getData(), $password) === $request->getSs1();
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    private function isSS2Valid(Request $request): bool
    {
        $response = $this->api->send(new HttpRequest('GET', self::ENDPOINT));
        if (200 === $response->getStatusCode() && false !== $publicKey = openssl_get_publickey($response->getBody())) {
            return 1 === openssl_verify($request->getData(), Encoder::decode($request->getSs2()), $publicKey);
        }

        return false;
    }
}
