<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\PayseraPlugin\Payum\Extension;

use Payum\Core\Extension\Context;
use Payum\Core\Extension\ExtensionInterface;
use Payum\Core\Request\Convert;
use Sylius\Component\Core\Model\PaymentInterface;

/**
 * Class PresetPaymentMethodExtension.
 */
final class PresetPaymentMethodExtension implements ExtensionInterface
{
    /**
     * @var string
     */
    private $paymentMethod;

    /**
     * PresetPaymentMethodExtension constructor.
     *
     * @param string $paymentMethod
     */
    public function __construct(string $paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * {@inheritdoc}
     */
    public function onPreExecute(Context $context): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function onExecute(Context $context): void
    {
    }

    /**
     * {@inheritdoc}
     */
    public function onPostExecute(Context $context): void
    {
        $request = $context->getRequest();
        if (false === $this->supports($request)) {
            return;
        }

        $request->setResult(array_replace($request->getResult(), ['paymentMethod' => $this->paymentMethod]));
    }

    /**
     * {@inheritdoc}
     */
    private function supports($request): bool
    {
        return $request instanceof Convert
            && $request->getSource() instanceof PaymentInterface
            && $request->getTo() === 'array';
    }
}
